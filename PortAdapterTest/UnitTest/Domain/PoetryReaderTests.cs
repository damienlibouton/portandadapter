using System;
using Xunit;
using PortAdapter.Domain;

namespace PortAdapterTest.UnitTest.Domain
{
    public class PoetryReaderTests
    {
        [Fact]
        public void GiveMeSomePoetry_ShouldNotReturnNull()
        {
            //Assign
            var poetryReader = new PoetryReader(null);

            //Act
            var poem = poetryReader.GiveMeSomePoetry();

            //Assert
            Assert.NotNull(poem);
        }
    }
}