﻿using System;
using PortAdapter.Infrastructure;
using PortAdapter.Application;
using PortAdapter.Domain;

namespace PortAdapter
{
    class Program
    {
        static void Main(string[] args)
        {
            IObtainPoem poetryLibrary = new PoetryLibraryFileAdapter(@"./verlaine.txt");

            IRequestVerse poetryReader = new PoetryReader(poetryLibrary);

            ConsoleAdapter consoleAdp = new ConsoleAdapter(poetryReader);

            consoleAdp.Run();
            
        }
    }
}
