namespace PortAdapter.Domain
{
    public class PoetryReader : IRequestVerse
    {
        private readonly IObtainPoem _poetryLibrary;
        public PoetryReader(IObtainPoem poetryLibrary)
        {
            _poetryLibrary = poetryLibrary;
        }
        public string GiveMeSomePoetry()
        {
            if (_poetryLibrary != null)
            {
                return _poetryLibrary.GetAPoem();
            }
            return "Pas de bibli...";
        }
    }
}