using PortAdapter.Domain;

namespace PortAdapter.Application
{
    public class ConsoleAdapter
    {
        private readonly IRequestVerse _poetryReader;
        public ConsoleAdapter(IRequestVerse poetryReader)
        {
            _poetryReader = poetryReader;
        }

        public void Run()
        {
            System.Console.WriteLine("Veux-tu un poème ? o/n");
            var reponse = System.Console.ReadLine();
            if (reponse.Equals("o"))
            {
                var poem = _poetryReader.GiveMeSomePoetry();
                System.Console.WriteLine(poem);
            }
            else 
            {
                System.Console.WriteLine("Yolo");
            }
        }

    }
}