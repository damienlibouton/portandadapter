using System.IO;
using PortAdapter.Domain;

namespace PortAdapter.Infrastructure
{
    public class PoetryLibraryFileAdapter : IObtainPoem
    {
        private readonly string _poem;
        public PoetryLibraryFileAdapter(string filePath)
        {
            _poem = File.ReadAllText(filePath);
        }
        public string GetAPoem()
        {
            return _poem;
        }
    }
}